#!/bin/bash
set -e
cd src
if [ -a compileResults.txt ]; then
	rm compileResults.txt -f
fi

#make opt &> compileResults.txt
make opt

# grepResults=$(cat compileResults.txt | grep -c "error:");

# echo "Test"
# if [ $grepResults == "0" ]; then
	clear
	echo "Running Program..."
	cd ../bin
	./game "$@"
# 	gdb --args ./game "$@"
	cd ../
# else
# 	clear
# 	echo "ERRORS:"
# 	file=$(cat compileResults.txt)
# 	echo "$file"
# fi
