#include "Model.h"
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "neuralnet.h"

using std::cerr;
using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::map;

vector<double> k_modelToStateVec(Model * m);
string vecToString(vector<double> v);
string to_string(double);
Matrix * removeColumn(Matrix & m, int col);


class kalmann
{
private:
	Model* m_model;
	NeuralNet m_transition, m_observation;
	vector<double> m_previousTrueState;
	vector<double> m_currentTrueState;
	vector<double> m_prediction;
	vector<double> m_trueDistance;
	bool m_first;
	Rand& m_random;
	double m_observationNoise;
	Matrix m_pK, m_pKProjected;
	Matrix m_kalmanGain;
	
	Matrix m_Q, m_R; // Transition, and observation error matrices.
	
protected:
	void initializePPast(Matrix & aK);
	
public:
	kalmann(Rand& r, double obsDev);
	void setModel(Model * m) { m_model = m; }
	void update(bool& flapped);
	void printTransitionError();
	void printObservationError(vector<double>& distances);
	
	void distanceWithNoise(double& topRight, double& bottomRight);
	
	void projectErrorCovariance();
	void computeKalmanGain();
	void updateEstimate();
	void updateCovariance();
};
