/*
  The contents of this file are dedicated by all of its authors, including

    Michael S. Gashler, Stephen Ashmore

  to the public domain (http://creativecommons.org/publicdomain/zero/1.0/).
*/

#include "Model.h"
#include <SDL/SDL.h>
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include "neuralnet.h"

using std::vector;
using std::string;
using std::map;

class View;

#ifndef LEARNERNET_H 
#define LEARNERNET_H 


vector<double> modelToStateVec(Model * m);
string to_string(int x);


class LearnerNet 
{
public:
	int exploits, explores;
	int m_exploreFlapRate, m_rewardScaler;
	NeuralNet Q_Net;
	double scale;
	int totalTime, totalReward;
	double learningRate, secondaryLearningRate;
	int iterations;
	Model* m_model;
	int ttl;
	int frame;
	bool* m_pKeepRunning;
	bool m_train, m_debug;
	double discount;
	bool a, m_showUpdates;
	Rand &r;

public:
	bool alive() { return a; }
	void setAlive(bool x) { a = x; }
	LearnerNet(Rand & random, int rewardScaler = 1,
			int exploreFlapRate = 50, bool debug = false);
	void setModel( Model* m ) {
		m_model = m;
	}
	virtual ~LearnerNet();
// 	void write(string file);
// 	void read(string file);

	void nextGame() { iterations++; }
	void resetExploreExploit() { explores = exploits = 0; }
	
	//void averageRewardTrain();
	void discountRewardTrain();

	void showUpdates(bool d) { m_showUpdates = d; }
	void setTrain(bool t) { m_train = t; }
	bool isTraining() { return m_train; }

	double computeReward(bool lose);

	bool exploit(bool&, double&, double&);
	bool explore(bool&, double&, double&);
	void updateQ(vector<double>&, vector<double>&,bool, double);
	
	void update();

protected:
	void handleKeyPress(SDLKey eKey, SDLMod mod);
};




#endif // CONTROLLER_H
