#include "kalmann.h"
#include <cmath>


string vecToString(vector<double> v)
{
	string returning = "";
	for ( unsigned int i = 0; i < v.size(); i++ ) {
		returning += to_string(v[i]);
		if ( i + 1 < v.size() ) returning += ",";
	}
	return returning;
	
}

vector<double> k_modelToStateVec(Model * m)
{
	vector<double> state;
	Bird &bird = m->bird;
	int birdy, birdvert;

	int birdx = bird.x;
	birdy = bird.y;
	birdvert = bird.vert_vel;
	
	state.push_back( birdy / 500.0 );
	state.push_back( birdx / 500.0 );
	state.push_back(birdvert / 50);
	
	
	return state;
}

kalmann::kalmann(Rand& r, double obsDev)
: m_transition(r), m_observation(r), m_random(r)
{
	m_observationNoise = obsDev;
	
	// state + action to state.
	m_transition.m_layers.push_back(new Layer(4,16));
	m_transition.m_layers.push_back(new Layer(16,3));
	m_transition.init();
	
	// state to distance from two corners
	m_observation.m_layers.push_back(new Layer(3,16));
	m_observation.m_layers.push_back(new Layer(16,2));
	m_observation.init();
	
	// Intialize the constant matrices for the transition and observation error:
	m_Q.setSize(3,3);
	for ( size_t i = 0; i < m_Q.rows(); i++ )
		for ( size_t j = 0; j < m_Q.cols(); j++ )
			if ( i == j )
				m_Q[i][j] = 0.03;
			else
				m_Q[i][j] = 0.0;
	// R should be same size as the jacobian of the observation model.
	m_R.setSize(2,2);
	for ( size_t i = 0; i < m_R.rows(); i++ )
		for ( size_t j = 0; j < m_R.cols(); j++ )
			if ( i == j )
				m_R[i][j] = 0.03;
			else
				m_R[i][j] = 0.0;
	
	
	m_first = true;
}

void kalmann::update(bool& flapped)
{
	// Find current state:
	m_currentTrueState = k_modelToStateVec(m_model);
	if ( m_first ) {
		m_previousTrueState = m_currentTrueState;
	}
	
	// Calculate Distance from two corners:
	double topRightDistance, bottomRightDistance;
	distanceWithNoise(topRightDistance, bottomRightDistance);
	
	// Debug messages:
	cerr << "Kalmann update method." << endl;
	cerr << "Perfect Current State: " << vecToString(m_currentTrueState) << endl;
	cerr << "Action: " << (flapped ? "Flapped" : "No Action") << endl;
	
	// Refine the Transition Model:
	m_previousTrueState.push_back(flapped);
	m_transition.refine(m_previousTrueState, m_currentTrueState, 0.1);
	m_prediction = m_transition.forward_prop(m_previousTrueState);
	printTransitionError();
	
	// Refine the observation model:
	vector<double> distances;
	distances.push_back(topRightDistance); distances.push_back(bottomRightDistance);
	m_observation.refine(m_currentTrueState, distances, 0.3);
	printObservationError(distances);
	m_trueDistance = distances;
	
	
	// Project error:
	projectErrorCovariance();
	
	// Update Measurements:
	computeKalmanGain();
	
	updateEstimate();
	
	updateCovariance();
	
	// Cleanup at the end, and setup for next update:
	m_previousTrueState = m_currentTrueState;
	
	if ( flapped )
		flapped = false;
	m_first = false;
}

Matrix * removeColumn(Matrix & m, int col) {
	Matrix* result = new Matrix(m.rows(), m.rows());
	for ( size_t i = 0; i < m.rows(); i++ )
		for ( size_t j = 0; j < m.rows(); j++ )
			result->row(i)[j] = m[i][j];
		
	return result;
}

void kalmann::updateEstimate() {
	Matrix xKBar;
	xKBar.setSize(0, 3);
	xKBar.newRow() = m_prediction;
	
	Matrix zK;
	zK.setSize(0, 2);
	zK.newRow() = m_trueDistance;
	
	vector<double> pred;
	pred = m_observation.forward_prop(m_prediction);
	
	Matrix hPred;
	hPred.setSize(0, 2);
	hPred.newRow() = pred;
	
	Matrix* inner = Matrix::subtract(zK, hPred);
	cerr << "Creating gained: Multiply kalman gain by inner." << endl;
	cerr << "Kalman gain size: " << m_kalmanGain.rows() << " x " << m_kalmanGain.cols() << endl;
	cerr << "Inner size: " << inner->rows() << " x " << inner->cols() << endl;
// 	Matrix* gained = Matrix::multiply(m_kalmanGain, *inner, false, false);
	
	Matrix* gained = new Matrix(1, 2);
	
// 	for ( size_t i = 0; i < m_kalmannGain.rows(); i++ )
// 	{
// 		
// 	}
	
// 	Matrix* result = Matrix::add(xKBar, *gained);
	
// 	cerr << "Location Estimate: " << endl;
// 	for ( int i = 0; i < result->rows(); i++ )
// 		for ( int j = 0; j < result->cols(); j++ )
// 			cerr << result->row(i)[j] << " ";
}

void kalmann::updateCovariance() {
	
}

void kalmann::computeKalmanGain() {
	Matrix hK;
	m_observation.jacobian(m_prediction, hK);
// 	Matrix *realHK = removeColumn(hK, hK.cols());
	Matrix *realHK = &hK;
	cerr << "hK rows: " << realHK->rows() << "\tcols: " << realHK->cols() << endl;
	
	Matrix * innerTerm1;
	cerr << "Hk * Pk-: " << endl;
	Matrix * innerTerm1Temp = Matrix::multiply(*realHK, m_pKProjected, false, false);
	cerr << "Previous term * HkTranspose: " << endl;
	innerTerm1 = Matrix::multiply(*innerTerm1Temp, *realHK, false, true);
	
	cerr << "Add to previous term the R noise." << endl;
	Matrix * inner = Matrix::add(*innerTerm1, m_R);
	cerr << "Multiply the projected by the transpose of realHK." << endl;
	Matrix * outer = Matrix::multiply(m_pKProjected, *realHK, false, true);
	cerr << "Pseudo inverse of inner." << endl;
	Matrix * innerT = inner->pseudoInverse();
	cerr << "Multiply the inverse inner by the previous matrix." << endl;
	Matrix * result = Matrix::multiply(*outer, *innerT, false, false);
	
	cerr << "Set size of kalman gain: " << endl;
	m_kalmanGain.setSize(0,0);
	m_kalmanGain.copyBlock(*result, 0, 0, result->rows(), result->cols());
}

void kalmann::projectErrorCovariance() {
	Matrix aK;
	m_transition.jacobian(m_previousTrueState, aK);
	// CUT THE LAST COLUMN:
	Matrix * realAK = removeColumn(aK, aK.cols());
// 	Matrix * realAK = &aK;
	cerr << "aK rows: " << realAK->rows() << "\tcols: " << realAK->cols() << endl;
	
	if ( m_first )
		initializePPast(*realAK);
	
	Matrix * temp1 = Matrix::multiply(*realAK, m_pK, false, false);
	Matrix * temp2 = Matrix::multiply(*temp1, *realAK, false, true);
	Matrix * temp3 = Matrix::add(*temp2, m_Q);
	
	m_pKProjected.setSize(0,0);
	m_pKProjected.copyBlock(*temp3, 0, 0, temp3->rows(), temp3->cols() );
}

void kalmann::initializePPast( Matrix & aK) {
	m_pK.copyMetaData(aK);
	m_pK.newRows(aK.rows());
	for ( int i = 0; i < m_pK.rows(); i++ )
		for ( int j = 0; j < m_pK.cols(); j++ )
			m_pK[i][j] = 0.0;
	m_pK[0][0] = 0.03;
	m_pK[1][1] = 0.03;
	m_pK[2][2] = 0.03;
}

void kalmann::distanceWithNoise(double& topRight, double& bottomRight) {
	double x = m_model->bird.x;
	double y = m_model->bird.y;
	
	topRight = sqrt((500-x)*(500-x) + (0-y)*(0-y));
	
	bottomRight = sqrt((500-x)*(500-x) + (500-y)*(500-y));
	
	topRight += m_random.normal() * m_observationNoise;
	bottomRight += m_random.normal() * m_observationNoise;
	
	topRight /= 750;
	bottomRight /= 750;
	topRight = topRight > 1.0 ? 1.0 : topRight;
	topRight = topRight < -1.0 ? -1.0 : topRight;
	bottomRight = bottomRight > 1.0 ? 1.0 : bottomRight;
	bottomRight = bottomRight < -1.0 ? -1.0 : bottomRight;
	
	cerr << "Distance: tr=" << topRight << " br=" << bottomRight << endl;
}

void kalmann::printObservationError(vector<double>& distances)
{
	vector<double> prediction = m_transition.forward_prop(m_currentTrueState);
	
	double sse = 0.0;
	for ( int i = 0; i < distances.size(); i++ ) {
		double diff = distances[i] - prediction[i];
		sse += diff * diff;
	}
	
	cerr << "Observation Model Error: " << sse << endl;
}

void kalmann::printTransitionError()
{
	vector<double> prediction = m_transition.forward_prop(m_previousTrueState);
	
	double sse = 0.0;
	for ( int i = 0; i < m_currentTrueState.size(); i++ ) {
		double diff = m_currentTrueState[i] - prediction[i];
		sse += diff * diff;
	}
	
	cerr << "Transition Model Error: " << sse << endl;
}
