/*
  The contents of this file are dedicated by all of its authors, including

    Michael S. Gashler, Stephen Ashmore

  to the public domain (http://creativecommons.org/publicdomain/zero/1.0/).
*/

#include "LearnerNet.h"
#include "View.h"
#include <iostream>
#include <iomanip>
using std::endl;
using std::cerr;
using std::setw;

string vecToString(vector<double>& v)
{
	string returning = "";
	for ( int i = 0; i < v.size(); i++ ) {
		returning += to_string(v[i]);
		if ( i + 1 < v.size() ) returning += ",";
	}
	return returning;
	
}

// Adding discretization of the model
vector<double> modelToStateVec(Model * m)
{
	vector<double> state;
	int distance1 = 0, distance2 = 0;
	int direction1 = 0, direction2 = 0;
	int length1 = 0, length2 = 0;
	int tube1x = 0, tube2x = 0;
	int tube1y = 0, tube2y = 0;
	int heightDiff = 0;

	Bird &bird = m->bird;
	int birdy, birdvert;

	birdy = bird.y;
	birdvert = bird.vert_vel;

	if ( m->tubes.size() >= 2 ) {
		// Find next two tubes in front of bird.
		size_t i = 0, count = 0, tube1Num = 0, tube2Num = 0;
		for ( i = 0; i < m->tubes.size(); i++ )
		{
			if ( bird.x < m->tubes[i]->x ) {
				// This tube is in front of the bird.
				if ( count == 0 )
				{
					tube1Num = i;
					count++;
				}
				else {
					count++;
					tube2Num = i;
					break;
				}
			}
		}

		Tube &tube1 = *m->tubes[tube1Num], &tube2 = *m->tubes[tube2Num];

		direction1 = tube1.up;
		direction2 = tube2.up;
		tube1x = tube1.x;
		tube2x = tube2.x;
		tube1y = tube1.y;
		tube2y = tube2.y;
		heightDiff = tube1.y - bird.y;
		distance1 = tube1.x - bird.x;
// 		distance2 = tube2.x - bird.x;
		if ( tube1.up ) 
			length1 = 500 - tube1.y;
		else	
			length1 = tube1.y;
		if ( tube2.up )
			length2 = 500 - tube2.y;
		else
			length2 = tube2.y;
		
	}
	else if ( m->tubes.size() == 1 ) {
		Tube & tube1 = *m->tubes[0];

		direction1 = tube1.up;
		tube1x = tube1.x;
		distance1 = tube1.x - bird.x;
		heightDiff = tube1.y - bird.y;
	
		tube1y = tube1.y;
		if ( tube1.up )
			length1 = 500 - tube1.y;
		else
			length1 = tube1.y;

	}

	
	state.push_back( birdy / 10 );
	state.push_back(birdvert);
	
	state.push_back(tube1x / 10);
	state.push_back(tube1y / 10);
	state.push_back(direction1);
	
	state.push_back(tube2x / 10);
	state.push_back(tube2y / 10);
	state.push_back(direction2);
	
	return state;
}

LearnerNet::LearnerNet(Rand & random, int rewardScaler = 1,
		int exploreFlapRate = 50, bool debug = false) 
: r(random), Q_Net(random)
{
	exploits = explores = 0;
	a = true;
	m_model = NULL;

	scale = 0.99;
	iterations = 1;
	totalTime = totalReward = 0;
	m_train = true;
	learningRate = 0.9;
	discount = 0.99;
	secondaryLearningRate = 0.1;
	m_exploreFlapRate = exploreFlapRate;
	
	m_rewardScaler = rewardScaler;
	m_debug = debug;
	m_showUpdates = false;
	
	Q_Net.m_layers.push_back(new Layer(9, 32));
	Q_Net.m_layers.push_back(new Layer(32, 1));
// 	Q_Net.m_layers.push_back(new Layer(8, 1));
	
	Q_Net.init();
}

double LearnerNet::computeReward(bool lose )
{
	double result = 0.0;
	
// 	if ( !lose )
// 		result = -2.0;
// 	else if ( m_model->score > prevscore )
// 		result = 1.0;
// // 	else if ( m_model->bird.y < 5 || m_model->bird.y > 45 )
// // 		result = -1.0;
// 	else
// 		result = 0.01;
	
	// Rewarding only on if it is alive:
	if ( lose )
		result = 0.00001;
	else
		result = -0.01;
	
	return result;
}

void LearnerNet::discountRewardTrain() {
	vector<double> state = modelToStateVec(m_model);
	vector<double> stateF = state;
	stateF.push_back(1);
	vector<double> stateN = state;
	stateN.push_back(0);
	
// 	cerr << "state size: " << state.size() << endl;
// 	cerr << "stateF size: " << stateF.size() << endl;
	
	double q1, q2;
	vector<double> resultVec;
	resultVec = Q_Net.forward_prop(stateF);
// 	cerr << "result vec size: " << resultVec.size() << endl;
	q1 = resultVec[0];
	
// 	cerr << "Hello" << endl;
	
	resultVec = Q_Net.forward_prop(stateN);
	q2 = resultVec[0];
	bool flap = false;
	// Q1 = Flap, Q2 = No Flap.

	// Pick which Action:
	// 0 is NOT FLAP
	// 1 is FLAP
	// determine probability:
	// Determine whether to explore or exploit:
	double p = 0.3;
	// OLD DECREASING PROBABILITY
// 	p = ( 400.0 )
// 	/ ( 800.0 
// 	+ (iterations * secondaryLearningRate) );
 	p = 0.3;
// 	p = 1.0;
// 	p = 0.5;
	
	
	// Explore if q factors are the same:
	if ( q1 == q2 )
		p = 1.0;
// 	p = 0.0;
		//+ (iterations) );
	// probability for exploring is p.
	
	
	
	double sample = r.uniform();
	//std::cerr << "sample: " << sample << " p= " << p*1000 << std::endl;
	bool explored = false, result;
	// EXPLORE
	if ( sample <= ( p )  ) {
		explores++;
		explored = true;
		result = explore(flap, q1, q2);
	}
	else {
		exploits++;
		result = exploit(flap, q1, q2);
	}
	
	a = result; // Setting alive variable.
	
	if ( m_debug ) {
		cerr << std::left << std::setprecision(2)
			<< std::showpoint << std::fixed;
		cerr << "S: " << setw(27) << vecToString(state)
			<< "F: " << setw(2) << flap << "\t"
			<< "QN: " << setw(6) << q1 << " QF: " << setw(6) << q2
			<< endl;
	}
	
	vector<double> next = modelToStateVec(m_model);

	double reward = computeReward(result);

	updateQ(state, next, flap, reward);

// 	iterations++;
// 	learningRate = 9000/(10000 + iterations);
// 	learningRate = learningRate + 0.0000001;
// 	learningRate = 0.99;
}

void LearnerNet::updateQ(vector<double>& state, vector<double>& next,
			bool flap, double reward)
{
	state.push_back((int)flap);
	
	vector<double> next0 = state;
	next0.push_back(0);
	vector<double> next1 = state;
	next1.push_back(1);
	
	double qState;
	vector<double> results;
	results = Q_Net.forward_prop(state);
	qState = results[0];
	
	double qNext0, qNext1;
	results = Q_Net.forward_prop(next0);
	qNext0 = results[0];
	results = Q_Net.forward_prop(next1);
	qNext1 = results[0];
	
	double best = qNext0 > qNext1 ? qNext0 : qNext1;
	
	double update =
		(1 - learningRate) *
		qState +
// 		learningRate *
		( reward + discount *
		( best ) );

// 	std::cerr << "reward = " << reward << "\t"
// 		<< "learning rate = " << learningRate << std::endl;
// 	if ( m_showUpdates )
// 	{
// 		std::cerr << "From state: " << state << "\t to "
// 			<< next << endl;
// 		std::cerr << "UPDATE = " << update << std::endl;
// 		std::cerr << "Reward = " << reward << std::endl;
// 	}
	// 	std::cin.get();
	vector<double> out;
// 	out.push_back(update);
	// trying simpler formula
	out.push_back(reward + discount * best); 
	for ( size_t i = 0; i < 10; i++ )
		Q_Net.refine(state, out, 0.1);
}

bool LearnerNet::exploit(bool & flap, double &q1, double & q2)
{
	if ( q2 > q1 )
		flap = true;
	else
		flap = false;

	if ( flap )
		m_model->flap();

	return m_model->update();
}

bool LearnerNet::explore(bool & flap, double &q1, double &q2)
{
	// Generate random flap or not:
	double pD = r.uniform();
	flap = 0;
	if ( pD <= 0.01 )
	{
		flap = 1;
		m_model->flap();
	}
	
	return m_model->update();
}

void LearnerNet::update()
{
	if ( m_train ) {
		//averageRewardTrain();
		discountRewardTrain();
	}
	else {
		// When not training:
		vector<double> state = modelToStateVec(m_model);
		double q1, q0;
		vector<double> state1 = state;
		state1.push_back(1);
		vector<double>results;
		results = Q_Net.forward_prop(state1);
		q1 = results[0];
		
		vector<double> state0 = state;
		state0.push_back(0);
		results = Q_Net.forward_prop(state0);
		q0 = results[0];
		
		if (q1 > q0 )
			m_model->flap();
		
		
		a = m_model->update();
	}
}

// void LearnerNet::read( string file ) {
// 	std::ifstream in;
// 	in.open(file.c_str());
// 	
// 	if ( !in )
// 	{
// 		std::cerr << "ERROR: Input file not found." << endl;
// 		exit(1);
// 	}
// 	
// 	string state;
// 	double qvalue;
// 	
// 	while ( in >> state >> qvalue ) {
// 		Q_FACTORS[state] = qvalue;
// 	}
// }
// 
// void LearnerNet::write( string file ) {
// 	std::ofstream out;
// 	out.open(file.c_str());
// 	
// 	for (map<string, double>::iterator it=Q_FACTORS.begin();
// 		 it!=Q_FACTORS.end(); ++it)
// 	{
// 		out << it->first << "\t" << it->second << endl;
// 	}
// }

LearnerNet::~LearnerNet()
{
}

