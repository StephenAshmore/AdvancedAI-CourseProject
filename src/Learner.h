/*
  The contents of this file are dedicated by all of its authors, including

    Michael S. Gashler, Stephen Ashmore

  to the public domain (http://creativecommons.org/publicdomain/zero/1.0/).
*/

#include "Model.h"
#include <SDL/SDL.h>
#include <string>
#include <fstream>
#include <iostream>
#include <map>

using std::vector;
using std::string;
using std::map;

class View;

const unsigned int G_LEARNER_Q_SIZE
	= 8000;
const unsigned int G_ITERMAX
	= 1000;

#ifndef LEARNER_H 
#define LEARNER_H 


string modelToState(Model * m);
string to_string(int x);
string to_string(double x);


class Learner 
{
public:
	int exploits, explores;
	int m_exploreFlapRate, m_rewardScaler;
	map<string, double> Q_FACTORS;
	double scale;
	int totalTime, totalReward;
	double learningRate, secondaryLearningRate;
	int iterations;
	Model* m_model;
	int ttl;
	int frame;
	bool* m_pKeepRunning;
	bool m_train, m_debug;
	double discount;
	bool a, m_showUpdates;
	Rand &r;

public:
	bool alive() { return a; }
	void setAlive(bool x) { a = x; }
	Learner(Model* m, bool* pKeepRunning, Rand & random);
	Learner(Rand & random, int rewardScaler = 1,
			int exploreFlapRate = 50, bool debug = false);
	void setModel( Model* m ) {
		m_model = m;
	}
	virtual ~Learner();
	void write(string file);
	void read(string file);

	void nextGame() { iterations++; }
	void resetExploreExploit() { explores = exploits = 0; }
	
	//void averageRewardTrain();
	void discountRewardTrain();

	void showUpdates(bool d) { m_showUpdates = d; }
	void setTrain(bool t) { m_train = t; }
	bool isTraining() { return m_train; }

	double computeReward(bool lose);

	bool exploit(bool&, double&, double&);
	bool explore(bool&, double&, double&);
	void updateQ(string , string , bool , double );

	void onChar(char c)
	{
                 std::cerr << "On Char " << c << std::endl;
                 if ( c == 'q' || c == 'Q' ) {
                         exit(1);
                }
	}

	void onSpecialKey(int key)
	{
                  std::cerr << "On Special " << key << std::endl;

	}

	void onMouseDown(int nButton, int x, int y)
	{
                  std::cerr << "On Mouse " << std::endl;

	}

	void onMouseUp(int nButton, int x, int y)
	{
	}

	bool onMousePos(int x, int y)
	{
		return false;
	}

	void update();

protected:
	void handleKeyPress(SDLKey eKey, SDLMod mod);
};




#endif // CONTROLLER_H
