/*
  The contents of this file are dedicated by all of its authors, including

    Michael S. Gashler, Stephen Ashmore

  to the public domain (http://creativecommons.org/publicdomain/zero/1.0/).
*/

#include "Learner.h"
#include "View.h"
#include <iostream>
#include <iomanip>
using std::endl;
using std::cerr;
using std::setw;

string to_string(int x) {
        std::stringstream ss;
        ss << x;
        string result;
        ss >> result;
        return result;
}

string to_string(double x) {
	std::stringstream ss;
	ss << x;
	string result;
	ss >> result;
	return result;
}

// Adding discretization of the model
string modelToState(Model * m)
{
	string state;
	int distance1 = 0, distance2 = 0;
	int direction1 = 0, direction2 = 0;
	int length1 = 0, length2 = 0;
	int tube1x = 0, tube2x = 0;
	int tube1y = 0, tube2y = 0;
	int heightDiff = 0;

	Bird &bird = m->bird;
	int birdy, birdvert;

	birdy = bird.y;
	birdvert = bird.vert_vel;

	if ( m->tubes.size() >= 2 ) {
		// Find next two tubes in front of bird.
		size_t i = 0, count = 0, tube1Num = 0, tube2Num = 0;
		for ( i = 0; i < m->tubes.size(); i++ )
		{
			if ( bird.x < m->tubes[i]->x ) {
				// This tube is in front of the bird.
				if ( count == 0 )
				{
					tube1Num = i;
					count++;
				}
				else {
					count++;
					tube2Num = i;
					break;
				}
			}
		}

		Tube &tube1 = *m->tubes[tube1Num], &tube2 = *m->tubes[tube2Num];

		direction1 = tube1.up;
		direction2 = tube2.up;
		tube1x = tube1.x;
		tube2x = tube2.x;
		tube1y = tube1.y;
		tube2y = tube2.y;
		heightDiff = tube1.y - bird.y;
		distance1 = tube1.x - bird.x;
// 		distance2 = tube2.x - bird.x;
		if ( tube1.up ) 
			length1 = 500 - tube1.y;
		else	
			length1 = tube1.y;
		if ( tube2.up )
			length2 = 500 - tube2.y;
		else
			length2 = tube2.y;
		
	}
	else if ( m->tubes.size() == 1 ) {
		Tube & tube1 = *m->tubes[0];

		direction1 = tube1.up;
		tube1x = tube1.x;
		distance1 = tube1.x - bird.x;
		heightDiff = tube1.y - bird.y;
	
		tube1y = tube1.y;
		if ( tube1.up )
			length1 = 500 - tube1.y;
		else
			length1 = tube1.y;

	}

	state = "";
	// COMPLEX ORIGINAL STATE:
	state += to_string(birdy / 10) + ",";
	state += to_string(birdvert) + ",";

	state += to_string(tube1x / 10) + ",";
	state += to_string(tube1y / 10) + ",";
	state += to_string(direction1) + ",";

	state += to_string(tube2x / 10) + ",";
	state += to_string(tube2y / 10) + ",";
	state += to_string(direction2);

	
/***		COMPLEX STATE TWO		***/
	// Bird state:
// 	state += to_string(birdy / 5) + ",";
// 	state += to_string(birdvert / 5) + ",";
	
	// Tube 1 State:
// 	state += to_string(tube1y / 5) + ",";
// 	state += to_string(tube1x / 5);
	
/***			SIMPLE STATE				***/
/*	This state contains just the distance	*
 * to the next pipe, and the difference in	*
 * height, and nothing else.				*/
// 	state += to_string(distance1 / 8) + ",";
// 	state += to_string(heightDiff / 8);
	
/***		SIMPLE STATE		***/
// 	state += to_string(birdy / 10) + ",";
// 	state += to_string(heightDiff / 10) + ",";
// 	state += to_string(birdvert / 5);

// 	state += to_string(birdy) + ",";
// 	state += to_string(heightDiff) + ",";
// 	state += to_string(birdvert) + ",";
// 	state += to_string(distance1 / 5);
// 	state += to_string(tube1x);
	
// 	std::cerr << "STATE: " << state << endl;
// 	std::cin.get();
	return state;
}

Learner::Learner(Model* model, bool* pKeepRunning, Rand & random)
: m_model(model), m_pKeepRunning(pKeepRunning),r(random)
{
	exploits = explores = 0;
	a = true;
	scale = 0.99;
	iterations = 1;
	totalTime = totalReward = 0;

	discount = 0.99;
	m_train = true;
	secondaryLearningRate = 0.1;
	m_exploreFlapRate = 50;
	
	m_rewardScaler = 1;
}

Learner::Learner(Rand & random, int rewardScaler = 1,
		int exploreFlapRate = 50, bool debug = false) 
: r(random)
{
	exploits = explores = 0;
	a = true;
	m_model = NULL;

	scale = 0.99;
	iterations = 1;
	totalTime = totalReward = 0;
	m_train = true;
	learningRate = 0.5;
	discount = 0.99;
	secondaryLearningRate = 0.1;
	m_exploreFlapRate = exploreFlapRate;
	
	m_rewardScaler = rewardScaler;
	m_debug = debug;
	m_showUpdates = false;
}

int prevscore = 0;

double Learner::computeReward(bool lose )
{
	double result = 0.0;
	if ( m_model->score == 0 )
		prevscore = 0;
	
// 	if ( !lose )
// 		result = -2.0;
// 	else if ( m_model->score > prevscore )
// 		result = 1.0;
// // 	else if ( m_model->bird.y < 5 || m_model->bird.y > 45 )
// // 		result = -1.0;
// 	else
// 		result = 0.01;
	
	// Rewarding only on if it is alive:
	if ( lose )
		result = 2.0;
	else
		result = -2.0;
	
	prevscore = m_model->score;
	
	return result;
}

/*void Learner::averageRewardTrain() {
	unsigned long state = 
		hashState(modelToState(m_model));
	int q1 = Q_FACTORS[state][0];
	int q2 = Q_FACTORS[state][1];

	// Pick which Action:
	// 0 is NOT FLAP
	// 1 is FLAP
	// determine probability:
	double p = (1000.0) / (2000.0 + iterations);	
	bool flap = true;
	bool greedy = true;
	if ( q2 < q1 ) {
		flap = false;
	}
	// Pull random number:
	if ( rand() % 100 >= ( p * 100 ) ) {
		flap = !flap;	
		greedy = false;
	}

		// FIND STATE:
		if ( flap )
			m_model->flap();

		bool result = m_model->update();
		a = result;
		// figure out next state:
		unsigned long next = 
			hashState(modelToState(m_model));

		// Update Q_Factors:
		double reward = computeReward(state, next, result);
		double update =
			(1 - learningRate) * 
			Q_FACTORS[state][flap] +
			learningRate *
			( reward - p + scale *
			( Q_FACTORS[next][0] > Q_FACTORS[next][1] ? Q_FACTORS[next][0] : Q_FACTORS[next][1] ) );
	
		Q_FACTORS[state][flap] = update;
	
		if ( greedy ) {
			totalReward = totalReward + reward;
			totalTime = totalTime + 1;
			
		}
	
		iterations++;
		learningRate = 90/(100 + iterations);
}*/

void Learner::discountRewardTrain() {
	string state = modelToState(m_model);
	string  stateF = state + ",1",
			stateN = state + ",0";
	double q1 = Q_FACTORS[stateN];
	double q2 = Q_FACTORS[stateF];
	bool flap = false;

	// Pick which Action:
	// 0 is NOT FLAP
	// 1 is FLAP
	// determine probability:
	// Determine whether to explore or exploit:
	double p = 0.3;
	// OLD DECREASING PROBABILITY
// 	p = ( 400.0 )
// 	/ ( 800.0 
// 	+ (iterations * secondaryLearningRate) );
 	p = 0.03;
// 	p = 1.0;
// 	p = 0.5;
	
	
	// Explore if q factors are the same:
	if ( q1 == q2 )
		p = 1.0;
// 	p = 0.0;
		//+ (iterations) );
	// probability for exploring is p.
	
	
	
	double sample = r.uniform();
	//std::cerr << "sample: " << sample << " p= " << p*1000 << std::endl;
	bool explored = false, result;
	// EXPLORE
	if ( sample <= ( p )  ) {
		explores++;
		explored = true;
		result = explore(flap, q1, q2);
	}
	else {
		exploits++;
		result = exploit(flap, q1, q2);
	}
	
	a = result; // Setting alive variable.
	
	if ( m_debug ) {
		cerr << std::left << std::setprecision(2)
			<< std::showpoint << std::fixed;
		cerr << "S: " << setw(27) << state
			<< "F: " << setw(2) << flap << "\t"
			<< "QN: " << setw(6) << q1 << " QF: " << setw(6) << q2
			<< endl;
	}
	
	string next = modelToState(m_model);

	double reward = computeReward(result);

	updateQ(state, next, flap, reward);

// 	iterations++;
// 	learningRate = 9000/(10000 + iterations);
// 	learningRate = learningRate + 0.0000001;
// 	learningRate = 0.99;
}

void Learner::updateQ(string state, string next,
			bool flap, double reward)
{
	state = state + "," + to_string((int)flap);
	
	string next0 = next + ",0", next1 = next + ",1";
	
	
	double update =
		(1 - learningRate) *
		Q_FACTORS[state] +
		learningRate *
		( reward + discount *
		( Q_FACTORS[next0] > Q_FACTORS[next1]
		? Q_FACTORS[next0]
		: Q_FACTORS[next1] ) );

// 	std::cerr << "reward = " << reward << "\t"
// 		<< "learning rate = " << learningRate << std::endl;
	if ( m_showUpdates )
	{
		std::cerr << "From state: " << state << "\t to "
			<< next << endl;
		std::cerr << "UPDATE = " << update << std::endl;
		std::cerr << "Reward = " << reward << std::endl;
	}
	// 	std::cin.get();
	Q_FACTORS[state] = update;
	// c / (1 - g)
	// c = 1.0
	// g = 0.95 discount rate
	// 1.0 / ( 1 - 0.95 )
	// 1.0 / 0.05
	// = 20
}

bool Learner::exploit(bool & flap, double &q1, double & q2)
{
	if ( q2 > q1 )
		flap = true;
	else
		flap = false;

	if ( flap )
		m_model->flap();

	return m_model->update();
}

bool Learner::explore(bool & flap, double &q1, double &q2)
{
	// Generate random flap or not:
	int p = rand() % 100;
	double pD = r.uniform();
	flap = 0;
	if ( pD <= 0.10 )
	{
		flap = 1;
		m_model->flap();
	}
	
	return m_model->update();
}

void Learner::update()
{
	if ( m_train ) {
		//averageRewardTrain();
		discountRewardTrain();
	}
	else {
		// When not training:
		string state = modelToState(m_model);
		if ( Q_FACTORS[state + ",1"] > Q_FACTORS[state + ",0"] )
			m_model->flap();
		a = m_model->update();
	}
}

void Learner::read( string file ) {
	std::ifstream in;
	in.open(file.c_str());
	
	if ( !in )
	{
		std::cerr << "ERROR: Input file not found." << endl;
		exit(1);
	}
	
	string state;
	double qvalue;
	
	while ( in >> state >> qvalue ) {
		Q_FACTORS[state] = qvalue;
	}
}

void Learner::write( string file ) {
	std::ofstream out;
	out.open(file.c_str());
	
	for (map<string, double>::iterator it=Q_FACTORS.begin();
		 it!=Q_FACTORS.end(); ++it)
	{
		out << it->first << "\t" << it->second << endl;
	}
}

Learner::~Learner()
{
}

void Learner::handleKeyPress(SDLKey key, SDLMod mod)
{
	exit(1);
}
