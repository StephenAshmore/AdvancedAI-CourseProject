/*
 * The contents of this file are dedicated by all of its authors, including
 * 
 *   Michael S. Gashler, Stephen Ashmore
 * 
 * to the public domain (http://creativecommons.org/publicdomain/zero/1.0/).
 */

#include <stdio.h>
#include <stdlib.h>
#include <exception>
#include <iostream>
#include <vector>
#include <iomanip>
#include <algorithm>
#ifdef WINDOWS
#	include <windows.h>
#else
#	include <unistd.h>
#endif
#include "Model.h"
#include "View.h"
#include "Controller.h"
#include "Learner.h"
#include "SArgReader.h"
#include "LearnerNet.h"
#include "kalmann.h"
#include "string.h"

using std::cerr;
using std::cout;
using std::vector;
using std::endl;
using std::cin;

int G_ITERATIONS = 50000;
bool DEBUG = false;


void mili_sleep(unsigned int nMiliseconds);

double mean(vector<int>& x) {
	int sum = 0;
	for ( size_t i = 0; i < x.size(); i++ )
		sum += x[i];
	return (double)sum / x.size();
}

void trainNet(SArgReader &args);
void test(SArgReader &args);
void train(SArgReader &args);
void kalmannFilter(SArgReader &args);

int main(int argc, char *argv[])
{
	SArgReader args(argc, argv);
	
	int nRet = 0;
	try
	{
//		kalmannFilter(args);
// 		trainNet(args);
		train(args);
//  		test(args);
	}
	catch(const std::exception& e)
	{
		cerr << e.what() << "\n";
		nRet = 1;
	}
	
	return nRet;
}

void kalmannFilter(SArgReader &args) {
	double standardDeviation = 1.0;
	while ( args.flagExists() ) {
		if ( args.checkFlag("-dev") )
		{
			if ( !args.nextDouble(standardDeviation) )
				standardDeviation = 1.0;
		}
		else {
			args.remove();
		}
	}
	
	
	Rand random(0);
	kalmann filter(random, standardDeviation);
	
	Model * model = new Model();
	
	filter.setModel(model);
	bool run = true;
	Controller c(model, &run);
	View v(*model, 500, 500);
	
	mili_sleep(1000);
	
	while (model->update())
	{
		c.update();
		filter.update(c.m_flapped);
		v.update();
		
		mili_sleep(30);
	}
	
	
}

void test(SArgReader &args) {
	string file = "model.ssd";
	int repeatNumber = 1;
	while ( args.flagExists() ) {
		if ( args.checkFlag("-debug") ) {
			DEBUG = true;
		}
		else if ( args.checkFlag("-file") ) {
			if ( !args.nextString(file) )
				file = "temp.ssd";
		}
		else if ( args.checkFlag("-repeat") ) {
			if ( !args.nextInt(repeatNumber) )
				repeatNumber = 1;
		}
		else {
			args.remove();
		}
	}
	
	/* 1 & 5 are no longer used */
	Rand random(12);
	Learner c(random, 1, 5, DEBUG);
	// Testing:
	Model *m;
	c.setTrain(false);
	
	
	
	cout << "Loading trained QFactor model from: " << file << endl;
	c.read(file);
	
	cout << "Running loaded QFactor Model." << endl;
	mili_sleep(2000);
	for ( int i = 0; i < repeatNumber; i++ ) {
		m = new Model();
		c.setModel(m);
		c.setAlive(true);
		View v(*m, 500, 500);
		while ( c.alive() ) {
			v.update();
			mili_sleep(30);
		
			c.update();
		}
		int score = m->score;

		cout << "Final Score: " << score << endl;
		delete m;
	}
}

void trainNet(SArgReader &args)
{
	int seed = 12, rewardScaler = 1,
	flapRate = 5;
	bool slow = false, view = false,
	infinite = false;
	while ( args.flagExists() ) {
		if ( args.checkFlag("-seed") ) {
			if ( !args.nextInt(seed) )
				seed = 12;
		}
		else if ( args.checkFlag("-iterations") ) {
			if ( !args.nextInt(G_ITERATIONS) )
				G_ITERATIONS = 10000;
		}
		else if ( args.checkFlag("-slow") ) {
			view = true;
			slow = true;
		}
		else if ( args.checkFlag("-view") ) {
			view = true;
		}
		else if ( args.checkFlag("-infinite") ) {
			infinite = true;
		}
		else if ( args.checkFlag("-debug") ) {
			DEBUG = true;
		}
		else {
			args.remove();
		}
	}
	
	Rand random(seed);
	LearnerNet c(random, rewardScaler,
			  flapRate, DEBUG);
	c.setTrain(true);
	
	int highScore = 0;
	int i = 0, countNonZeroes = 0;
	vector<int> scores;
	int totalExploits, totalExplores;
	totalExploits = totalExplores = 0;
	
	for ( i = 0; i < G_ITERATIONS; i++ ) {
		Model *m = new Model();
				View v(*m, 500, 500);
		c.setModel(m);
		
		c.setAlive(true);
		
		while ( c.alive() ) {
						if ( view ) 
							v.update();
			if ( slow )
				mili_sleep(30);
			
			c.update();
		}
		c.nextGame();
		// 		cerr << "DEATH" << endl;
		int score = m->score;
		
		scores.push_back(score);
		if ( score > 0 )
			countNonZeroes++;
		
		if ( score > highScore )
			highScore = score;
		if ( i % 1000 == 0 ) {
			if ( infinite ) i = 0;
			
			cout << "Game: " << i / 1000 << "k."
			<< " Mean: " << mean(scores)
			<< " NonZeros: " << countNonZeroes
			<< " High Score: " << highScore
			<< " evx: " << c.explores << "v" << c.exploits
			<< endl;
			
			totalExplores += c.explores;
			totalExploits += c.exploits;
			
			// reset debugging/output variables:
			c.explores = 0;
			c.exploits = 0;
			countNonZeroes = 0;
			c.resetExploreExploit();
			scores.clear();
		}
		delete m;
	}
	
	
	
	Model *m = new Model();
	View v(*m, 500, 500);
	c.setTrain(false);
	c.setModel(m);
	c.setAlive(true);
	
	cerr << "Explores versus Exploits: " << endl;
	cerr << c.explores << " v. " << c.exploits << endl;
	cerr << "TESTING TIME" << endl;
	mili_sleep(2000);
	
	while ( c.alive() ) {
		v.update();
		mili_sleep(30);
		
		c.update();
	}
	int score = m->score;
	if ( score > highScore )
		highScore = score;
	cout << "Final Score: " << score << endl;
	cout << "High Score: " << highScore << endl;
	delete m;
	
	/* Writing to model to file */
	
}

void train(SArgReader &args)
{
	int seed = 12, rewardScaler = 5,
		flapRate = 50;
	bool slow = false, view = false,
		infinite = false;
	string file = "temp.ssd";
	while ( args.flagExists() ) {
		if ( args.checkFlag("-seed") ) {
			if ( !args.nextInt(seed) )
				seed = 12;
		}
		else if ( args.checkFlag("-rewardScale") ) {
			if ( !args.nextInt( rewardScaler ) )
				rewardScaler = 1;
		}
		else if ( args.checkFlag("-flapRate") ) {
			if ( !args.nextInt( flapRate ) )
				flapRate = 50;
		}
		else if ( args.checkFlag("-iterations") ) {
			if ( !args.nextInt(G_ITERATIONS) )
				G_ITERATIONS = 10000;
		}
		else if ( args.checkFlag("-slow") ) {
			view = true;
			slow = true;
		}
		else if ( args.checkFlag("-view") ) {
			view = true;
		}
		else if ( args.checkFlag("-infinite") ) {
			infinite = true;
		}
		else if ( args.checkFlag("-debug") ) {
			DEBUG = true;
		}
		else if ( args.checkFlag("-file") ) {
			if ( !args.nextString(file) )
				file = "temp.ssd";
		}
		else {
			args.remove();
		}
	}
	
	Rand random(seed);
	Learner c(random, rewardScaler,
			  flapRate, DEBUG);
	c.setTrain(true);
	
	int highScore = 0;
	int i = 0, countNonZeroes = 0;
	vector<int> scores;
	
 	for ( i = 0; i < G_ITERATIONS; i++ ) {
// 		if ( i + 1 == G_ITERATIONS )
// 			c.showUpdates(true);
		
		Model *m = new Model();
// 		View v(*m, 500, 500);
		c.setModel(m);
		
		c.setAlive(true);
		
		while ( c.alive() ) {
// 			if ( view ) 
// 				v.update();
			if ( slow )
				mili_sleep(30);
			
			c.update();
		}
		c.nextGame();
// 		cerr << "DEATH" << endl;
		int score = m->score;
		
		scores.push_back(score);
		if ( score > 0 )
			countNonZeroes++;
		
		if ( score > highScore )
			highScore = score;
		if ( i % 1000 == 0 ) {
			if ( infinite ) i = 0;
			
			cout << "Game: " << i / 1000 << "k."
			<< " Mean: " << mean(scores)
			<< " NonZeros: " << countNonZeroes
			<< " High Score: " << highScore
			<< " evx: " << c.explores << "v" << c.exploits
			<< endl;
			
			// reset debugging/output variables:
			countNonZeroes = 0;
			c.resetExploreExploit();
			scores.clear();
			if ( i % 100000 == 0 ) {
				string name = to_str(i) + file;
				c.write(name.c_str());
			}
		}
		delete m;
	}
	
	
	
	Model *m = new Model();
	View v(*m, 500, 500);
	c.setTrain(false);
	c.setModel(m);
	c.setAlive(true);
	
	cerr << "Explores versus Exploits: " << endl;
	cerr << c.explores << " v. " << c.exploits << endl;
	cerr << "TESTING TIME" << endl;
	mili_sleep(2000);

	while ( c.alive() ) {
		v.update();
		mili_sleep(30);
		
		c.update();
	}
	int score = m->score;
	if ( score > highScore )
		highScore = score;
	cout << "Final Score: " << score << endl;
	cout << "High Score: " << highScore << endl;
	delete m;
	
	/* Writing to model to file */
	c.write(file.c_str());
}

void mili_sleep(unsigned int nMiliseconds)
{
	#ifdef WINDOWS
	MSG aMsg;
	while(PeekMessage(&aMsg, NULL, WM_NULL, WM_NULL, PM_REMOVE))
	{
		TranslateMessage(&aMsg);
		DispatchMessage(&aMsg);
	}
	SleepEx(nMiliseconds, 1);
	#else
	nMiliseconds ? usleep(nMiliseconds*1024) : sched_yield(); // it is an error to sleep for more than 1,000,000
	#endif
}
